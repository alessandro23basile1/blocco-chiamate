<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contact extends Model
{
    use HasFactory;
    protected $fillable = [
        'data',
        'type',
        'name',
        'surname',
        'adress',
        'adress_number',
        'city',
        'telephone',
        'problem',
        'day',
        'time',
        ];
}
