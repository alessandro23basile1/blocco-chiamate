<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\contact;

class BloccoChiamateForm extends Component
{
    public $contactId = null;
    public $data;
    public $type;
    public $name;
    public $surname;
    public $adress;
    public $adress_number;
    public $city;
    public $telephone;
    public $problem;
    public $day;
    public $time;


    protected $rules = [
        'data' => 'required',
        'type' => 'required',
        'name' => 'required|min:3',
        'surname' => 'required|min:3',
        'adress' => 'required|min:3',
        'adress_number' => 'required',
        'city' => 'required|min:3',
        'telephone' => 'required',
        'problem' => 'required|min:3',
        'day' => 'required',
        'time' => 'required',
    ];
        protected $messages = [
            
            'data' => 'Il campo date non può essere vuoto',
            'type' => 'Il campo type non può essere vuoto',
            'name' => 'Il campo name non può essere vuoto',
            'surname' => 'Il campo surname non può essere vuoto',
            'adress'=> 'Il campo adress non può essere vuoto',
            'adress_number' => 'Il campo adress_number non può essere vuoto',
            'city' => 'Il campo city non può essere vuoto',
            'telephone'=> 'Il campo telephone non può essere vuoto',
            'problem' => 'Il campo problem non può essere vuoto',
            'day'=> 'Il campo day non può essere vuoto',
            'time'=> 'Il campo time non può essere vuoto',
        ];

        protected $listeners = [
            'edit' => 'edit',
        ];

        public function mount(){
            if($this->contactId){
                $contact = contact::find($this->contactId);
                $this->data = $contact->data;
                $this->type = $contact->type;
                $this->name = $contact->name;
                $this->surname = $contact->surname;
                $this->adress = $contact->adress;                
                $this->adress_number = $contact->adress_number;
                $this->city = $contact->city;
                $this->telephone = $contact->telephone;
                $this->problem = $contact->problem;
                $this->day = $contact->day;
                $this->time = $contact->time;
            }
        }


    public function store()
    {
        $this->validate();
         
        if($this->contactId){
            $contact = contact::findOrFail($this->contactId);
            $contact->data = $this->data;
            $contact->type = $this->type;
            $contact->name = $this->name;
            $contact->surname = $this->surname;
            $contact->adress = $this->adress;
            $contact->adress_number = $this->adress_number;
            $contact->city = $this->city;
            $contact->telephone = $this->telephone;
            $contact->problem = $this->problem;
            $contact->day = $this->day;
            $contact->time = $this->time;           
            $contact->save();
            
            session()->flash('message', 'Contatto modificato   correttamente');
           
        }
       
        else
        $this->validate();{
            contact::create([    
                'data' => $this->data,  
                'type' => $this->type,    
                'name' => $this->name,
                'surname' => $this->surname,
                'adress' => $this->adress,
                'adress_number' => $this->adress_number,
                'city' => $this->city,
                'telephone' => $this->telephone,
                'problem' => $this->problem,
                'day' => $this->day,
                'time' => $this->time,
               
            ]); 
           
            session()->flash('message', 'Contatto inserito correttamente');
        }
        


        $this->emitTo('contacts-list','updateData');
        $this->clearForm(); 
            
    }    

    public function edit(contact $contact){
        $this->contactId = $contact->id;
        $this->data = $contact->data;
        $this->type = $contact->type;
        $this->name = $contact->name;
        $this->surname = $contact->surname;
        $this->adress = $contact->adress;
        $this->adress_number = $contact->adress_number;
        $this->city = $contact->city;
        $this->telephone = $contact->telephone;
        $this->problem = $contact->problem;
        $this->day = $contact->day;
        $this->time = $contact->time;

    }

       
    private function clearForm(){
      ;
        $this->data = '';
        $this->type =  '';
        $this->name =  '';
        $this->surname = '';
        $this->adress = '';
        $this->adress_number = '';
        $this->city = '';
        $this->telephone = '';
        $this->problem = '';
        $this->day =  '';
        $this->time = '';

        
    }
    
    public function render()
    {
        return view('livewire.blocco-chiamate-form');
    }
}
