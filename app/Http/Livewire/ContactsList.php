<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\contact;
use Illuminate\Queue\Listener;

class ContactsList extends Component
{

    public $contacts;
    
    protected $listeners = [
        'updateData' => 'updateData',
    ];
    
    public function mount()
    {
         $this->updateData();
    
    } 

    public function updateData(){
      $this->contacts = contact::all();
     
    }

    public function delete(contact $contact){
        $contact->delete();
        $this->updateData();
    }

    public function render()
    {        
        
        return view('livewire.contacts-list');
    }
}
