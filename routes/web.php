<?php

use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[PageController::class,'home'])->name('home');
// Route::get('crea/{contact}',[PageController::class,'create'])->name('create');
Route::get('visualizza',[PageController::class,'show'])->name('show');

Route::get('create/{contact?}', function ($contact = null ) {

    
    return view('bloccoChiamate', compact('contact'));
})->name('create');

Route::get('/blocco', function () {
    return view('bloccoChiamate');
});

Route::get('/posta', function () {
    return view('post');
});
