<div>

    <h1>Agenda Appuntamenti</h1>
      
    <form wire:submit.prevent="store" class="row g-3">

        <div class="col-md-6">
            <label  class="form-label">Data chiamata</label>
            <input wire:model.lazy="data" type="text" placeholder="Data chiamata" class="form-control" >
          </div>
          <div class="col-md-6">
              <label  class="form-label">Tipo di apparecchio</label>
              <input wire:model.lazy="type" type="text" placeholder="Tipo di apparecchio" class="form-control" >
            </div>

        <div class="col-md-6">
          <label  class="form-label">Nome</label>
          <input wire:model.lazy="name" type="text" placeholder="Inserisci il nome" class="form-control" >
        </div>
        <div class="col-md-6">
            <label  class="form-label">Cognome</label>
            <input wire:model.lazy="surname" type="text" placeholder="Inserisci il cognome" class="form-control" >
          </div>
        <div class="col-6">
          <label  class="form-label">Indirizzo</label>
          <input wire:model.lazy="adress" type="text" class="form-control"  placeholder="Via">
        </div>
        <div class="col-4">
            <label  class="form-label">Numero Civico</label>
            <input wire:model.lazy="adress_number" type="number" class="form-control" >
          </div>

        <div class="col-md-2">
            <label for="inputState" class="form-label">Città</label>
            <select wire:model.lazy="city" class="form-select">
              <option selected>Seleziona città</option>
              <option>Anzio</option>
              <option>Nettuno</option>
              <option>Lavinio</option>
            </select>
          </div>

          <div class="col-md-6">
            <label class="form-label">Telefono</label>
            <input wire:model.lazy="telephone" type="text" placeholder="Telefono" class="form-control" >
          </div>       

          <div class="col-md-6">
            <label  class="form-label">Difetto lamentato</label>
            <input wire:model.lazy="problem" type="textarea" placeholder="Difetto lamentato" class="form-control" >
          </div>


          <div class="col-md-4">
            <label  class="form-label">Giorno Appuntamento</label>
            <select wire:model="day" class="form-select">
              <option selected>Seleziona giorno</option>
              <option>Lunedì</option>
              <option>Martedi</option>
              <option>Mercoledì</option>
              <option>Giovedì</option>
              <option>Venerdì</option>
              <option>Sabato</option>
            </select>
          </div>

          <div class="col-4">
            <label  class="form-label">Ora</label>
            <input wire:model.lazy="time" type="time" class="form-control" >
          </div>
         
       
        <div class="col-12">
          <button type="submit" class="btn btn-primary ">Salva</button>
        </div>

 
        @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
     
        
      </div>
    </div>
  </div>
</div>
               
               
            
                @if($contactId) 
                <span class="text-info">Stai modificando il contatto con ID:  {{$contactId}}</span>
                @endif
                
                @error('data') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('type') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('surname') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('surname') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('adress') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('adress_number') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('city') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('telephone') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('problem') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('day') <span class="text-danger">{{ $message }}</span> @enderror
                <br>
                @error('time') <span class="text-danger">{{ $message }}</span> @enderror

        


            
            </form>


            
         
</div>


