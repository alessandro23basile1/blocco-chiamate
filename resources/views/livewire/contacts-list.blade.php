{{-- <table class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>DATE</th>
            <th>TYPE</th>
            <th>NAME</th>
            <th>SURNAME</th>
            <th>ADRESS</th>
            <th>ADRESS_NUMBER</th>
            <th>CITY</th>
            <th>TELEPHONE</th>
            <th>PROBLEM</th>
            <th>DAY</th>
            <th>TIME</th>
           
        </tr>
    </thead>
    <tbody>
        @foreach ($contacts as $contact)
        <tr>
            <td>{{$contact->id}}</td>
            <td>{{$contact->data}}</td>
            <td>{{$contact->type}}</td>            
            <td>{{$contact->name}}</td>
            <td>{{$contact->surname}}</td>
            <td>{{$contact->adress}}</td>
            <td>{{$contact->adress_number}}</td>
            <td>{{$contact->city}}</td>
            <td>{{$contact->telephone}}</td>
            <td>{{$contact->problem}}</td>
            <td>{{$contact->day}}</td>
            <td>{{$contact->time}}</td>
            <td class="text-end">
                <a href="{{route('create',$contact)}}" class="btn btn-info btn-sm"
                        wire:click="$emitTo('blocco-chiamate-form','edit',{{$contact->id}})">Modifica</a>

                <button class="btn btn-danger btn-sm" wire:click="delete( {{$contact->id }})">Elimina</button>

            </td>
        </tr>
        @endforeach
    </tbody>         --}}
       {{-- @dd($contacts) --}}
        
           
       

    @foreach ($contacts as $contact)

    <div class="container cardcontact d-flex mx-4 my-4">
        <div class="row">
            <div class="col-12">
                <div class="card" >
                    <div class="card-body my-2">
                      <h5 class="card-title">Chiamata n°{{$contact->id}}</h5>
                        
                      

                      <div class="input-group my-2">
                        <span class="input-group-text">Nome e Cognome</span>
                        <input type="text" placeholder="{{$contact->name}}" aria-label="First name" class="form-control text-capitalize fw-bold">
                        <input type="text"  placeholder="{{$contact->surname}}" aria-label="Last name" class="form-control text-capitalize fw-bold">
                      </div>

                      <div class="input-group my-2">
                        <span class="input-group-text">Indirizzo</span>
                        <input type="text" placeholder="{{$contact->adress}} n°{{$contact->adress_number}}" aria-label="First name" class="form-control text-capitalize fw-bold">
                        <input type="text"  placeholder="{{$contact->city}}" aria-label="Last name" class="form-control text-capitalize fw-bold">
                      </div>

                      <div class="input-group input-group-sm mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Telefono</span>
                        <input placeholder="{{$contact->telephone}}" type="text" class="form-control fw-bold text-capitalize" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                      </div>

                      <div class="input-group input-group-sm mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Giorno</span>
                        <input placeholder="{{$contact->data}}" type="text" class="form-control fw-bold text-capitalize" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                      </div>

                      <div class="input-group my-2">
                        <span class="input-group-text">Data e Giorno</span>
                        <input type="text" placeholder="{{$contact->data}}" aria-label="First name" class="form-control text-capitalize fw-bold">
                        <input type="text"  placeholder="{{$contact->day}}" aria-label="Last name" class="form-control text-capitalize fw-bold">
                      </div>

                      

                      <hr class="border border-primary border-3">
                      

                      <h6 class="card-subtitle mb-2 text-muted">Difetto Lamentato</h6>
                      <p class="card-text text-uppercase">{{$contact->problem}}</p>
                      <hr class="border border-primary border-3">
                      <a href="{{route('create',$contact)}}" class="btn btn-warning mx-3" 
                      wire:click="$emitTo('blocco-chiamate-form','edit',{{$contact->id}})">Modifica</a>
                      <a wire:clik="delete" class="btn btn-danger mx-3" >Elimina</a>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    @endforeach
    

</table>
