<x-main>
    <div class="container mt-4">
        <div class="row">
            <div class="col md-6 mx-auto">                

                <div class="mt-5">
                    <livewire:show-posts/>
                </div>
            </div>

        </div>
    </div>
</x-main>