<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('data');
            $table->string('type');
            $table->string('name',50);  
            $table->string('surname',50);
            $table->string('adress',50);
            $table->string('adress_number');
            $table->string('city',50);
            $table->string('telephone');
            $table->string('problem',150);
            $table->string('day');
            $table->string('time');
           
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contacts');
    }
};
